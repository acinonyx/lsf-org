###############################
Documenting with GitLab Web IDE
###############################

.. contents::
   :local:
   :backlinks: none

********
Overview
********

This guide outlines the process for contributing to Read the Docs documentation
projects, such as this one, via the GitLab Web IDE.
To learn more about GitLab Web IDE visit the `GitLab documentation pages
<https://docs.gitlab.com/ee/user/project/web_ide/>`_.

*************
Prerequisites
*************

Before diving into this How-To, you should be acquainted with the following key
areas and tools:

#. `Overview of Version control
   <https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control>`_
#. `Git version control system basics
   <https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F>`_
#. `Introduction to reStructuredText
   <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
#. Libre Space Foundation contribution guide
#. `GitLab Merge Requests
   <https://docs.gitlab.com/ee/user/project/merge_requests/>`_

*****
Steps
*****

Locate the page
===============

.. |edit-on-gitlab| image:: /_images/edit-on-gitlab.png

* Navigate to the page you intend to contribute improvements or changes to.
* In the top right corner, locate and select the |edit-on-gitlab| link. This
  action redirects you to the page's source code.


Edit the page
=============

.. |gitlab-edit-button| image:: /_images/gitlab-edit-button.png

* Press the |gitlab-edit-button| button, then choose ``Open in Web IDE`` to proceed with
  modifications.
* Update the page content as needed. The Web IDE allows the editing of
  additional pages simultaneously.

Commit changes
==============

.. |gitlab-ide-source-control| image:: /_images/gitlab-ide-source-control.png
.. |gitlab-ide-commit-to| image:: /_images/gitlab-ide-commit-to.png

* To finalize edits, press ``Ctrl+Shift+G``, or alternatively, click the
  |gitlab-ide-source-control| Source Control icon. This opens the Source Control panel, highlighting all modified
  files with an ``M`` indicator.
* Compose a concise commit message that encapsulates your updates. Adhere to
  the LSF contribution guidelines: include a sign-off line post an empty line
  following your message's subject or body. Example: ``Signed-off-by: Name Surname <user@email.com>``.
* Confirm your changes by clicking the dropdown on the |gitlab-ide-commit-to| button, opting
  for ``Commit to new branch`` from the available choices.
* Assign a distinctive name to the new branch and confirm with ``Enter``.

Create a merge request
======================

.. |gitlab-create-mr| image:: /_images/gitlab-create-mr.png

* A popup message soon appears on the bottom left corner of the IDE
  prompting you with an option to initiate a merge request; select this to
  proceed.
* The merge request form pre-populates with relevant details. Simply click
  |gitlab-create-mr| to move forward.

Evaluation and reviewing
========================

* An automatic CI pipeline commences, evaluating the quality and integrity of
  your submitted changes.
* Monitor the pipeline's progress. Address any detected issues by either
  updating your current branch or initiating a new merge request (MR).
* Await a review of your contributions or proceed to self-merge, in alignment
  with the LSF merge request management protocols.

.. image:: /_images/docs-with-gitlab-ide.gif
