:orphan:

###############################
Hardware development guidelines
###############################

.. contents::
   :local:
   :backlinks: none

********
Overview
********

This guide refers to practices that is followed for hardware projects.
It contains 3 sections:

#. design
#. fabrication
#. assembly

Each of the sections refer to a different stage of hardware development.

******
Design
******

Repository
==========

The repository of a hardware project must include:

#. License file for example, `CERN Open Hardware Licence Version 2 - Strongly
   Reciprocal
   <https://gitlab.com/librespacefoundation/sidloc/sidloc-schematic/-/blob/master/LICENSE>`_
#. Libre Space Foundation `Contribution Guide
   <https://gitlab.com/librespacefoundation/sidloc/sidloc-schematic/-/blob/master/CONTRIBUTING.md>`_
#. Git ignore files for different kind of programs, like KiCad, FreeCAD and
   QUCS.
#. In case of CAD files, for example FreeCAD, it is recommended to use `Git LFS
   <https://docs.gitlab.com/ee/topics/git/lfs/>`_
#. For project management follow, `recommended Mode of Operation for Libre
   Space Foundation projects
   <https://docs.libre.space/en/latest/projects/project-management.html>`_
#. An (under test) method to keep track of issues with epics and/or milestones
   is followed at `SIDLOC project - issue
   <https://gitlab.com/librespacefoundation/lsf-org/-/issues/32>`_ and some
   `notes
   <https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-org/-/issues/177>`_

KiCad and FreeCAD workflow
==========================

#. KiCad workflow for multiple boards is under test in `SIDLOC project
   <https://gitlab.com/librespacefoundation/sidloc>`_ and an example of the
   method is given below.
#. FreeCAD workflow uses `Git LFS - File Locking
   <https://docs.gitlab.com/ee/topics/git/file_management.html#file-locks>`_

An example KiCAD workflow for multiple boards (from SIDLOC project)
===================================================================

Consider the case where someone wants to make some schematic and PCB changes in
one of the SIDLOC sub-projects (for example RFFE).
Described below is a series of steps to perform these changes according to the
"Breadboards Prototyping Workflow" scheme.

Change the common schematic
---------------------------

First thing would be to clone either a branch of SIDLOC Schematic project or a
forked repository::

   git clone link_to_forked_repo

Then, changes can be made on the specific sheet of the SIDLOC schematic
project.
In this case, the RFFE sheet (which corresponds to the rffe.kicad_sch
file). When the changes are ready, they shall be pushed to the branch or the
forked repository. ::

   git status

*The git status command may show changes in other files, but one has to add
only the file of interest.* ::

   git add rffe.kicad_sch (and any other files containing PURPOSEFUL changes)
   git status

*Use git status again to confirm that the correct files have been changed* ::

   git commit -s -m "message"
   git push (appropriate remote and branch name  e.g. "origin master" for pushing on the master branch of the remote repo named "origin")

After pushing, a merge request can be submitted, reviewed, and merged into the
upstream SIDLOC Schematic repository.

Change the specific schematic and project
-----------------------------------------

Now, one needs to clone the specific project they want to work on, again in a
branch or fork, including the :code:`sidloc-schematic` submodule. ::

   git clone --recurse-submodules link_to_forked_repo

In case one had already cloned into the specific repository when the changes on
the SIDLOC Schematic were made, they can navigate into the
:code:`sidloc-schematic` folder and use::

   git pull

*A note : this folder is linked to the LSF SIDLOC schematic repository, not any
forked repository.
This can be seen in the .gitmodules file.
So, one has to wait for the changes to be merged into the LSF SIDLOC Schematic
repository.
One could also change the .gitmodules file to pull changes form the forked
SIDLOC Schematic repository, but this is not particularly useful as corrections
might occur during the merge request review process.
So it is best to wait for the changes to be merged.*

After successfully pulling the changes in the :code:`sidloc-schematic` folder,
it is time to modify the board-specific components of the project.
If not already done, follow the preceding steps to set-up the project-specific
schematic :

#. Create a hierarchical sheet whose filename points to the corresponding file
   in the :code:`sidloc-schematic` project.
   In this case that would be :code:`sidloc-schematic/rffe.kicad_sch`.
   **The internals of this sheet should NOT be changed from the specific
   project.
   They are changed in the SIDLOC-Schematic with the preceding steps and
   after pulling the changes in the ``sidloc-schematic`` folder, this sheet
   automatically is updated.**
#. Check that the hierarchical sheet pins are the same as those in the common
   SIDLOC-Schematic.
   These are added manually (they are not updated when pulling changes in the
   :code:`sidloc-schematic`).
#. Add any connectors and parts that are specific to your board implementation
   of the SIDLOC project.
   Remember that the common SIDLOC-Schematic has no specific board
   implementation - one might as well put all subsystems in a big board.
   So, any connectors like the PQ9 connector are considered project-specific
   and are placed outside the hierarchical sheets.

When changes to the breadboard project are complete, then they can be
committed, pushed on a fork/branch and then merged upon review. ::

   git status

*Make sure to add only the project-specific files (not files from the
sidloc-schematic folder) that were PURPOSEFULLY changed (for example changes in
the PCB)* ::

   git add sidloc-rffe.kicad_pcb
   git status

Commit and push::

   git commit -s -m "Message"
   git push (appropriate remote and branch name  e.g. "origin master" for pushing on the master branch of the remote repo named "origin")

Finally, submit a merge request.

General checks
--------------

* All ports required by either the common project or the breadboard projects
   are exposed as hierarchical labels
* ERC passes on all projects; unused hierarchical ports must be set to “not
   connected”.
*  There is no net naming conflicts
*  There is no component naming conflicts (annotations)

Integrating LSF library in a KiCad project
=============================================

LSF maintains a symbol, footprint, and 3D model library for KiCAD under the
`lsf-kicad-lib repository
<https://gitlab.com/librespacefoundation/lsf-kicad-lib>`_.
For LSF projects, it is good to regularly merge the symbols/footprints created
for the project into the lsf-kicad-lib; contribution guidelines are written in
the repository's README.
To use the library in a design, the following steps are proposed (also
summarized in the README) :

* Clone lsf-kicad-lib anywhere in your computer (choose a writeable location to
  allow modifications to the library)
* Under :code:`Preferences -> Configure Paths` set a new environment variable
  named :code:`LSF_KICAD_LIB` and set the path to the location where
  lsf-kicad-lib is cloned
* Under :code:`Preferences -> Manage Symbol Libraries` add :code:`lsf-kicad` as
  a global symbol library using the
  :code:`${LSF_KICAD_LIB}/lsf-kicad.kicad_sym` path
* Under :code:`Preferences -> Manage Footprint Libraries` add
  :code:`lsf-kicad-lib` as a global footprint library using the
  :code:`${LSF_KICAD_LIB}/lsf-kicad-lib.pretty` path


Electronics parts
=================

#. An inventory with electronic parts is kept in LSF lab. For that reason it is
   recommended to select parts from inventory that the threshold of minimum
   order is more than 20 (more details TBA)
#. Parts like MCUs, PSUs etc are preferred to re-used for all the projects
#. Preferred `vendors <https://docs.libre.space/en/latest/operation/procurement.html>`_
#. For Board to wire connectors the Molex - PicoBlade and Hirose - DF11 series
   are preferred
#. A `Libre Space Foundation KiCad Library
   <https://gitlab.com/librespacefoundation/lsf-kicad-lib>`_ exists.
   It is advised to use parts from it (or KiCAD libraries).
   In case that a part does not exist a merge request in to library is more
   than welcome
#. To export a BOM, for example for Mouser, use either
   `bom_csv_grouped_by_value` and from generated .csv use the second table of
   follow a guide: `The Generation of Bill of Material Files
   <https://www.raypcb.com/the-method-to-generate-centroid-file-and-bom-from-kicad/>`_

KiCad preferred settings
========================

#. Schematic grid size: 1.27mm
#. PCB Logos: project logo, license logo and Libre Space Foundation logo.
#. CI for `KiCad (Under development)
   <https://gitlab.com/librespacefoundation/sidloc/sidloc-org/-/issues/123>`_
#. Add Fiducial points that help stencil alignment in stencil printer

FreeCAD preferred settings
==========================

#. For fabrication drawings use `Libre Space Foundation templates
   <https://gitlab.com/librespacefoundation/templates/freecad>`_

Releases
========

The release scheme is under development and testing in `SatNOGS COMMS project
<https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/merge_requests/176>`_

Fabrication
-----------

PCB ordering
============

#. Preferred `vendors <https://docs.libre.space/en/latest/operation/procurement.html>`_
   are to be followed
#. Tag a release on the hardware repository and write all production details
   like `this
   <https://gitlab.com/librespacefoundation/cronos-rocket/sky-runner-payload/sky-runner-hw/-/tags/v1.0.0>`_
   .
#. Ping people with access according to Procurement policy.
#. Note: Stencil size (maximum) 180x220 mm (due to stencil printer)

Assembly
--------

PCB assembly
============

Libre Space Foundation uses `hackerspace.gr laboratory
<https://www.hackerspace.gr/>`_ with:

#. Reflow oven, TBD
#. Pick and Place, TBD
#. Stencil printer, TBD


Mechanical parts assembly
=========================

TBD

Cables assembly
===============

TBD
