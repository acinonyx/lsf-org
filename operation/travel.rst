######
Travel
######

This page provides comprehensive guidelines for all Libre Space Foundation core contributors undertaking travel on behalf of the organization. Adherence to these guidelines ensures smooth, efficient, and safe travel experiences while representing the Foundation professionally.

**1. Pre-Travel Authorization & Planning**

There is no formal pre-travel authorization process, yet PMs should be notified for coordination and budget allocation. Assuming you are traveling for a specific project, the PM of that project should be aware and approving it.

**2. Booking Travel Arrangements**

- **Flights:**
    - **Class of Travel:**  Economy class is the standard for all flights.
    - **Direct Flights:** Prioritize direct flights whenever feasible to minimize travel time and potential disruptions.
    - **Baggage:**  Adhere to airline baggage allowances to avoid extra fees. If carrying specialized equipment, ensure it complies with airline regulations.
    - **Booking:** Send an email to Shadi with flight details to be booked and project to be charged.
- **Accommodation:**
    - **Booking Channels:**  Use the approved booking platforms (airbnb) or contact Shadi to secure special accommodation arrangements.
    - **Selection Criteria:**
       1. Proximity to Event/Meeting Venue: Minimize travel time and costs.
       2. Safety and Security: Choose places in safe areas.
       3. Amenities: Ensure access to Wi-Fi, workspace, and other necessary amenities.

.. admonition:: How to setup a business airbnb account

    1. Create a regular individual airbnb account
    2. Go to ``Account > Travel for Work`` and add your LSF email
    3. Go to ``Booking permissions`` and make sure that you are verified for all profile details
    4. Add Shadi's email in the section ``People who can book trips for you``

* **Ground Transportation:**
    * **Cost-Effective Options:**  Utilize public transportation, ride-sharing services or airport shuttles whenever possible.
    * **Rental Cars:**  If a rental car is essential, contact Shadi to arrange for booking.
    * **Local Transportation:**  Familiarize yourself with local transportation options (metro, bus, tram) at your destination. Consider purchasing travel passes for cost-effective travel.

**3. Travel Expenses and Reimbursement**

* **Expense Reporting:**
    * **Timely Submission:**  Submit all expense reports within **14 days** of completing your trip. Late submissions may result in processing delays.
    * **Designated System:**  Scan all your receipts with an app (for example Adobe Scan on Android) and send all individual PDF to ``pay@libre.space`` with descriptions and for which project was the expenses for.
* **Acceptable Expenses:**  The following expenses are typically eligible for reimbursement:
    * **Airfare:**  Economy class.
    * **Accommodation:**  Hotel costs within the per diem rate (TBD).
    * **Ground Transportation:**  Public transportation, ride-sharing, rental car costs.
    * **Meals:**  Within the per diem limits (TBD)
    * **Conference/Event Fees:**  Registration, workshops, materials.
    * **Business-Related Expenses:**  Wi-Fi, phone calls, printing, stationery.
* **Non-Reimbursable Expenses:**
    * **Personal Expenses:**  Souvenirs, gifts, personal shopping.
    * **Entertainment:**  Unless directly related to business and pre-approved.
    * **Fines and Penalties:**  Traffic tickets, parking violations.

.. tip::
    To avoid paying and then waiting reimbursement, for airfare, accommodation, and rentals prefer pre-booking and payment with Shadi.

**4. Travel Safety and Security**

* **Travel Advisories:**  Stay informed about travel advisories, safety alerts, and security concerns for your destination. Consult official sources like `WHO Travel Advice <https://www.who.int/travel-advice>`_.
* **Health and Insurance:**
    * **Travel Insurance:** TBD
    * **Vaccinations:**  Consult a healthcare professional or travel clinic well in advance to determine necessary vaccinations or health precautions for your destination.
    * **Medications:**  Carry an adequate supply of any required medications, along with a copy of your prescription.
* **Personal Safety:**
    * **Be vigilant:**  Maintain awareness of your surroundings, especially in crowded areas or unfamiliar environments.
    * **Secure valuables:**  Safeguard your passport, money, and electronic devices. Utilize hotel safes when available.
    * **Share itinerary:**  Inform a trusted person about your travel plans and provide regular updates.
* **Emergency Contacts:**
    * **Foundation Emergency Contact:**  Shadi and Pierros
    * **Local Emergency Services:**  Familiarize yourself with local emergency numbers (police, ambulance, fire department).

**5. Representing the Libre Space Foundation**

* **Professionalism:**  Maintain a professional demeanor at all times during your travels. You are an ambassador for the Foundation.
* **Code of Conduct:**  Adhere to the Libre Space Foundation :doc:`../collaboration/code-of-conduct`, which outlines expected behaviors and ethical standards.
* **Branding and Communication:**
    * **Foundation Materials:**  Utilize Foundation-branded materials (presentations, brochures, attire) when appropriate to promote the organization.
    * **Social Media:**  When sharing travel experiences on social media, be mindful of representing the Foundation positively. Use official hashtags and handles when relevant.
