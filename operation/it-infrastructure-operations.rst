############################
IT infrastructure operations
############################

.. contents::
   :local:
   :backlinks: none

********
Overview
********

IT infrastructure operations are the various activities related to the
operation and maintenance of the organization's IT infrastructure.
They encompass all the essential components of an organization's IT
environment, including hardware, software, data centers, networks, servers,
storage, and other infrastructure elements.
The IT infrastructure operations focus on ensuring the availability,
reliability, security, and performance of the IT systems and services that
support the organization.
It also involves the implementation of processes and procedures to manage and
monitor the infrastructure components to deliver uninterrupted services to
end-users.

********
Services
********

A variety of IT services are used within the organization.
These services can be either self-hosted or externally hosted.

Communication
=============

Email (libre.space)
-------------------

+-------------------------------------------+
| .. rst-class::                            |
|    wy-text-center                         |
|                                           |
| Info table                                |
+=============+=============================+
| URL         | https://webmail.migadu.com/ |
+-------------+-----------------------------+
| Active      | Yes                         |
+-------------+-----------------------------+
| Self-hosted | No                          |
+-------------+-----------------------------+
| SSO         | No                          |
+-------------+-----------------------------+

Email (other)
-------------

+-------------------------------------------+
| .. rst-class::                            |
|    wy-text-center                         |
|                                           |
| Info table                                |
+=============+=============================+
| URL         | https://mail.libre.space/   |
+-------------+-----------------------------+
| Active      | Yes                         |
+-------------+-----------------------------+
| Self-hosted | Yes                         |
+-------------+-----------------------------+
| SSO         | No                          |
+-------------+-----------------------------+

Collaboration
=============

Cloud
-----

+------------------------------------------+
| .. rst-class::                           |
|    wy-text-center                        |
|                                          |
| Info table                               |
+=============+============================+
| URL         | https://cloud.libre.space/ |
+-------------+----------------------------+
| Active      | Yes                        |
+-------------+----------------------------+
| Self-hosted | Yes                        |
+-------------+----------------------------+
| SSO         | No                         |
+-------------+----------------------------+

Web conferencing
----------------

+---------------------------------------------------+
| .. rst-class::                                    |
|    wy-text-center                                 |
|                                                   |
| Info table                                        |
+=============+=====================================+
| URL         | https://cloud.libre.space/apps/bbb/ |
+-------------+-------------------------------------+
| Active      | Yes                                 |
+-------------+-------------------------------------+
| Self-hosted | No                                  |
+-------------+-------------------------------------+
| SSO         | No                                  |
+-------------+-------------------------------------+

Development
===========

DevOps platform
---------------

+-----------------------------------+
| .. rst-class::                    |
|    wy-text-center                 |
|                                   |
| Info table                        |
+=============+=====================+
| URL         | https://gitlab.com/ |
+-------------+---------------------+
| Active      | Yes                 |
+-------------+---------------------+
| Self-hosted | No                  |
+-------------+---------------------+
| SSO         | No                  |
+-------------+---------------------+

Distribution packages repository
--------------------------------

+-------------------------------------------+
| .. rst-class::                            |
|    wy-text-center                         |
|                                           |
| Info table                                |
+=============+=============================+
| URL         | https://build.opensuse.org/ |
+-------------+-----------------------------+
| Active      | Yes                         |
+-------------+-----------------------------+
| Self-hosted | No                          |
+-------------+-----------------------------+
| SSO         | No                          |
+-------------+-----------------------------+

Python packages repository
--------------------------

+---------------------------------+
| .. rst-class::                  |
|    wy-text-center               |
|                                 |
| Info table                      |
+=============+===================+
| URL         | https://pypi.org/ |
+-------------+-------------------+
| Active      | Yes               |
+-------------+-------------------+
| Self-hosted | No                |
+-------------+-------------------+
| SSO         | No                |
+-------------+-------------------+

Docker images repository
-------------------------

+---------------------------------------+
| .. rst-class::                        |
|    wy-text-center                     |
|                                       |
| Info table                            |
+=============+=========================+
| URL         | https://hub.docker.com/ |
+-------------+-------------------------+
| Active      | Yes                     |
+-------------+-------------------------+
| Self-hosted | No                      |
+-------------+-------------------------+
| SSO         | No                      |
+-------------+-------------------------+

Application error tracking system
---------------------------------

+----------------------------------+
| .. rst-class::                   |
|    wy-text-center                |
|                                  |
| Info table                       |
+=============+====================+
| URL         | https://sentry.io/ |
+-------------+--------------------+
| Active      | Yes                |
+-------------+--------------------+
| Self-hosted | No                 |
+-------------+--------------------+
| SSO         | No                 |
+-------------+--------------------+

PCB design visual collaboration platform
----------------------------------------

+----------------------------------+
| .. rst-class::                   |
|    wy-text-center                |
|                                  |
| Info table                       |
+=============+====================+
| URL         | https://cadlab.io/ |
+-------------+--------------------+
| Active      | Yes                |
+-------------+--------------------+
| Self-hosted | No                 |
+-------------+--------------------+
| SSO         | No                 |
+-------------+--------------------+

Documentation
=============

Sphinx hosting platform
-----------------------

+----------------------------------------+
| .. rst-class::                         |
|    wy-text-center                      |
|                                        |
| Info table                             |
+=============+==========================+
| URL         | https://readthedocs.org/ |
+-------------+--------------------------+
| Active      | Yes                      |
+-------------+--------------------------+
| Self-hosted | No                       |
+-------------+--------------------------+
| SSO         | No                       |
+-------------+--------------------------+

Online LaTeX editor
-------------------

+-------------------------------------+
| .. rst-class::                      |
|    wy-text-center                   |
|                                     |
| Info table                          |
+=============+=======================+
| URL         | https://overleaf.com/ |
+-------------+-----------------------+
| Active      | Yes                   |
+-------------+-----------------------+
| Self-hosted | No                    |
+-------------+-----------------------+
| SSO         | No                    |
+-------------+-----------------------+

Social
======

Websites
--------

+------------------------------------+
| .. rst-class::                     |
|    wy-text-center                  |
|                                    |
| Info table                         |
+=============+======================+
| URL         | https://libre.space/ |
+-------------+----------------------+
| Active      | Yes                  |
+-------------+----------------------+
| Self-hosted | Yes                  |
+-------------+----------------------+
| SSO         | No                   |
+-------------+----------------------+

Community forum
---------------

+----------------------------------------------+
| .. rst-class::                               |
|    wy-text-center                            |
|                                              |
| Info table                                   |
+=============+================================+
| URL         | https://community.libre.space/ |
+-------------+--------------------------------+
| Active      | Yes                            |
+-------------+--------------------------------+
| Self-hosted | Yes                            |
+-------------+--------------------------------+
| SSO         | Yes                            |
+-------------+--------------------------------+

Event management system
-----------------------

+-------------------------------------------+
| .. rst-class::                            |
|    wy-text-center                         |
|                                           |
| Info table                                |
+=============+=============================+
| URL         | https://events.libre.space/ |
+-------------+-----------------------------+
| Active      | Yes                         |
+-------------+-----------------------------+
| Self-hosted | Yes                         |
+-------------+-----------------------------+
| SSO         | No                          |
+-------------+-----------------------------+

Lab
===

Inventory
---------

+-------------------------------------------------+
| .. rst-class::                                  |
|    wy-text-center                               |
|                                                 |
| Info table                                      |
+=============+===================================+
| URL         | https://inventory.hackerspace.gr/ |
+-------------+-----------------------------------+
| Active      | Yes                               |
+-------------+-----------------------------------+
| Self-hosted | Yes                               |
+-------------+-----------------------------------+
| SSO         | No                                |
+-------------+-----------------------------------+

ELN
---

+----------------------------------------+
| .. rst-class::                         |
|    wy-text-center                      |
|                                        |
| Info table                             |
+=============+==========================+
| URL         | https://eln.libre.space/ |
+-------------+--------------------------+
| Active      | Yes                      |
+-------------+--------------------------+
| Self-hosted | Yes                      |
+-------------+--------------------------+
| SSO         | No                       |
+-------------+--------------------------+

Auxiliary
=========

Status page system
------------------

+-------------------------------------------+
| .. rst-class::                            |
|    wy-text-center                         |
|                                           |
| Info table                                |
+=============+=============================+
| URL         | https://status.libre.space/ |
+-------------+-----------------------------+
| Active      | Yes                         |
+-------------+-----------------------------+
| Self-hosted | Yes                         |
+-------------+-----------------------------+
| SSO         | No                          |
+-------------+-----------------------------+

URL shortener
-------------

+----------------------------------------------+
| .. rst-class::                               |
|    wy-text-center                            |
|                                              |
| Info table                                   |
+=============+================================+
| URL         | * https://s.libre.space/admin/ |
|             | * https://satno.gs/admin/      |
+-------------+--------------------------------+
| Active      | Yes                            |
+-------------+--------------------------------+
| Self-hosted | Yes                            |
+-------------+--------------------------------+
| SSO         | No                             |
+-------------+--------------------------------+

Projects
========

SatNOGS
-------

+----------------------------------------------------+
| .. rst-class::                                     |
|    wy-text-center                                  |
|                                                    |
| Info table                                         |
+=============+======================================+
| URLs        | * https://network.satnogs.org/       |
|             | * https://db.satnogs.org/            |
|             | * https://network-dev.satnogs.org/   |
|             | * https://db-dev.satnogs.org/        |
|             | * https://dashboard.satnogs.org/     |
|             | * https://warehouse.satnogs.org:8086/|
|             |                                      |
+-------------+--------------------------------------+
| Active      | Yes                                  |
+-------------+--------------------------------------+
| Self-hosted | Yes                                  |
+-------------+--------------------------------------+
| SSO         | Yes (except data warehouse)          |
+-------------+--------------------------------------+
