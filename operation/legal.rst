#####
Legal
#####

Libre Space Foundation has a legal team and External Legal Counsel to look after all things legal (i.e contracts, compliance etc)

You can reach out to the team for any requests, guidance, or issue at legal@libre.space


Contracts
=========

When needed LSF enters into contractual agreement with various entities or individuals for projects and/or services.
Sample templates for contracts can be found here in `English <https://cloud.libre.space/s/LFaZGQzz56wX2Mf>`_ & `Greek <https://cloud.libre.space/s/nc9Y5z4M84sr8DL>`_.
