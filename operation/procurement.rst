###########
Procurement
###########


For various project and activity needs, contributors of LSF need to procure
various items or services. Here is a brief outline and some guiding principles

.. note::
   The following procedures apply to major items, that is items costing more than 500 EUR. For minor purchases please see :doc:`../operation/accounting`.

************************************
Procurement process for common items
************************************

The term "common items" refers to items that are readily available on the market and for which there are no specific requirements or questions to be answered by the manufacturer.
Their price is provided by the vendor and there is no need to request a quote (in that case see the relevant section below).
A good example of common items are electronic components.

#. Identify the need and think about possible future evolution of this need.
#. Check with Lab (contacting Aris) on possible existing stock
#. Identify the best vendor possible taking into account lower price but also a
   prioritization with regards to vendor location. The priority is as follows:

  #. EU vendor with valid VAT VIES number
  #. Greek vendor with VAT number
  #. Non-EU vendor with local VAT number (avoid if possible!)
  #. Any vendor without a VAT number (avoid if possible!)

#. Request a pro-forma invoice to be issued (unless specific vendor below) to
   LSF Company details.
#. Forward to pay@libre.space for payment via bank transfer or via a card
   purchase link. Always prefer bank transfer over PayPal.
#. Once paid, you receive confirmation from the people behind pay@
#. Once the items arrive, scan and forward the invoice to bills@libre.space mentioning the project the procurement was done for.

.. note::
   Whenever available, on the "order number" field add a name indicative of the project and date to help with invoice identification and analytical accounting.


*******************************************
Procurement process involving quote request
*******************************************

As is the case with many items, the vendor does not disclose the price on their website and a quote is needed.
A quote may also be required when there is the need to ask for specific information about the product.
In this case, the following information exchange with the vendor is performed.

#. LSF sends an RFQ to which the manufacturer replies with a quote. Other questions about the product are included in the RFQ.

#. If there is a high degree of certainty about the purchase of the product, LSF can ask for additional, more specific information (for example materials or components list).
#. If LSF is ready to proceed to purchase, a PO (Purchase Order) is sent to the vendor, to which the vendor replies with a pro-forma invoice.
#. Finally, LSF completes the payment and an invoice is received together with the product.

*****************************************
Procurement information for flight models
*****************************************

When procuring components/subsystems for flight models, some extra information shall be requested from the manufacturer.
Since the depth and quantity of the requested information indicate a further commitment into the procurement, they should be requested at a second stage, not together with the initial quotation request.
Internally, those are requested for quality assurance and risk evaluation purposes, while externally they are sometimes requested by the contracting agencies.
The information to be requested are described below.

#. Components List

   This is basically the Bill Of Materials of the subsystem and it is requested to enable LSF to investigate the suitability of individual components in case it is needed.

#. Materials List

   This is more generic than the Components List and includes the materials used in the product.
   It can be used to assess whether the product contains materials unsuitable for the operational environment of the project or materials that can hinder the performance of other subsystems.

#. Inspection Process

   This information request is concerned with the quality inspection procedures to which the manufacturer has subjected the product.

#. Qualifications

   This refers to the standards to which the product complies.

You can copy/paste the following text to request the preceding information:
```
Please provide with your quote or proforma invoice the following detailed information:
a) Detailed components list, b) Materials list, c) Inspection Processes applied, d) Qualifications of items
```


****************
Specific vendors
****************

The following vendors have been used extensively in the past and should be
preferred since there is a special setup (See notes)

+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
|                      Vendor                      |               Username               |                 Access                 |                             Notes                             |
+==================================================+======================================+========================================+===============================================================+
| `Amazon <https://www.amazon.de/>`_               | individual accounts                  | All core contributors                  | Business account, Purchases with policies                     |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `ebay <https://www.ebay.com/>`_                  | procurement@libre.space              | Pierros, Shadi                         | Account for invoices                                          |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Mouser <https://www.mouser.com/>`_              | librespacefoundation                 | Pierros, Agis, Dimitris M, Shadi       | Business account, Details completed.                          |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Digikey <https://www.digikey.gr/>`_             | procurement@libre.space              | Pierros, Shadi                         | Business account, Details completed                           |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Farnell <https://export.farnell.com/>`_         | librespace                           | Pierros, Shadi                         | Business account, Details completed                           |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `TME <https://www.tme.eu/>`_                     | pierros_librespace                   | Pierros                                | Business account, Details completed                           |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Mini Circuits <https://www.minicircuits.com/>`_ |                                      | Pierros                                | Details for export control added                              |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Discomp <https://www.discomp.cz/>`_             |                                      | Pierros, Aris                          | Business discount and details entered                         |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `JLC PCB <https://jlcpcb.com/>`_                 | procurement@libre.space              | Pierros, Agis, Aris, Dimitris M, Shadi | Business account, Details completed. Choose DDP for shipping. |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `LCSC <https://www.lcsc.com/>`_                  | procurement@libre.space              | Pierros, Agis, Aris, Dimitris M, Shadi | Business account, Details completed                           |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Eurocircuits <https://www.eurocircuits.com/>`_  | individual accounts                  | Pierros, Dimitris M                    | Business account, ask for individual account invites          |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Mapbox <https://www.mapbox.com/>`_              | ops@libre.space                      | Pierros, Shadi                         | Ops account                                                   |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `AirBnB <https://www.airbnb.com/>`_              | pierros@libre.space                  | Pierros, Shadi                         | Business account for invoices                                 |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Fiverr <https://www.fiverr.com/>`_              | individual accounts                  | Pierros, Shadi, Nikoletta, Nestoras    | Business account for invoices, payment setup                  |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Reichelt <https://www.reichelt.com/>`_          | pierros                              | Pierros, Shadi                         | Business account for invoices                                 |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Framework <https://frame.work/>`_               | pierros@libre.space                  | Pierros                                | Business account for invoices                                 |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `PCBWay <https://www.pcbway.com/>`_              | procurement@libre.space              | Pierros, Agis, Aris, Dimitris M, Shadi | Business account for invoices                                 |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `Skroutz <https://www.skroutz.gr/>`_             | Libre Space Foundation               | Shadi, Pierros                         | Business account for invoices                                 |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `ESDshop <https://www.esdshop.eu/>`_             | procurement@ and individual accounts | Shadi, Pierros, Manthos                | Business account for invoices                                 |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+
| `ELESHOP.EU <https://www.eleshop.eu/>`_          | procurement@                         | Shadi, Pierros, Manthos                | Business account for invoices                                 |
+--------------------------------------------------+--------------------------------------+----------------------------------------+---------------------------------------------------------------+

If you require a purchase from the vendors in the preceding table, please reach out directly to
someone with access. Same goes for requesting access to those vendors.

A special alias (procurment@libre.space) is created for common accounts. People with access to procurement@ are Pierros, Agis, Aris, Dimitris M, Shadi


********************
Legal entity details
********************

For legal entity details (company numbers and info) see :doc:`../operation/entity`.
