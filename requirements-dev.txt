doc8~=0.11.0
sphinx-autobuild==2021.3.14
sphinx-lint~=0.8.0
tox~=4.11.0
vale~=3.0.0.0
