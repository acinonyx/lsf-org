######################
Communication channels
######################

Libre Space Foundation uses various channels, technologies and methods to
communicate among its members, projects and to the world.

***********************
External communications
***********************

For a list of external communication channels and specifics around the
organization of the Communications Team check out the `Communications
Organizational Project <https://gitlab.com/librespacefoundation/lsf-comms-org>`_ .

**********************
Project communications
**********************

Different project employ different types of communications:

Electronic addresses
====================

+---------------+------------------------+------------------------------------------------------------+
|    Project    |         email          |                        Description                         |
+===============+========================+============================================================+
| LSF - Generic | info@libre.space       | Central email for all LSF related inquiries                |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | shop@libre.space       | Online shop related emails                                 |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | ops@libre.space        | Infrastructure related emails                              |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | ssa@libre.space        | Space Situational Awareness team email                     |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | moc@libre.space        | Mission Operations Center email                            |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | all@libre.space        | All LSF core members alias (restricted to LSF emails only) |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | staff@libre.space      | All LSF employees (restricted to LSF emails only)          |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | acct@libre.space       | Financial and Accounting project team email                |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | bd@libre.space         | Business Development team email                            |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | bills@libre.space      | Incoming email for Odoo accounting system                  |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | components@libre.space | Alias used in vendor accounts for procurements             |
+---------------+------------------------+------------------------------------------------------------+
| LSF - Generic | pay@libre.space        | Alias used for requesting payment                          |
+---------------+------------------------+------------------------------------------------------------+

Real-time communications
========================

+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| Project        | Channel                                                                                          | Description                                        | Access       |
+================+==================================================================================================+====================================================+==============+
| LSF - Generic  | `#librespace:matrix.org <https://app.element.io/?#/room/#librespace:matrix.org>`_                | Generic channel for all LSF projects               | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| LSF - Generic  | `#lsf-ops:matrix.org <https://app.element.io/?#/room/#lsf-ops:matrix.org>`_                      | Infrastructure team channel                        | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| LSF - Generic  | `#lsf-core:matrix.org <https://app.element.io/?#/room/#lsf-core:matrix.org>`_                    | Channel for all LSF Core members                   | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| LSF - Generic  | `#librespace-comms:matrix.org <https://app.element.io/?#/room/#librespace-comms:matrix.org>`_    | Communication team channel                         | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| LSF - Generic  | `#librespace-board:matrix.org <https://app.element.io/?#/room/#librespace-board:matrix.org>`_    | LSF Board channel                                  | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| SatNOGS        | `#satnogs:matrix.org <https://app.element.io/?#/room/#satnogs:matrix.org>`_                      | SatNOGS project main channel                       | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-dev:matrix.org <https://app.element.io/?#/room/#satnogs-dev:matrix.org>`_              | SatNOGS Developers channel                         | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-missions:matrix.org <https://app.element.io/?#/room/#satnogs-missions:matrix.org>`_    | Missions using SatNOGS                             | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-operators:matrix.org <https://app.element.io/?#/room/#satnogs-operators:matrix.org>`_  | Channel for SatNOGS scheduling and operators       | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-comms:matrix.org <https://app.element.io/?#/room/#satnogs-comms:matrix.org>`_          | SatNOGS COMMS CubeSat subsystem project            | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-optical:matrix.org <https://app.element.io/?#/room/#satnogs-optical:matrix.org>`_      | SatNOGS Optical Ground Station development         | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| SIDLOC         | `#sidloc:matrix.org <https://app.element.io/?#/room/#sidloc:matrix.org>`_                        | SIDLOC spacecraft ID and localization project      | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| PQ9ISH         | `#PQ9ISH:matrix.org <https://app.element.io/?#/room/#PQ9ISH:matrix.org>`_                        | PQ9ISH PocketQube standard and compatible projects | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| QUBIK          | `#qubik:matrix.org <https://app.element.io/?#/room/#qubik:matrix.org>`_                          | QUBIK and PICOBUS, PocketQube LSF projects         | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| stvid          | `#stvid:matrix.org <https://app.element.io/?#/room/#stvid:matrix.org>`_                          | stvid, optical SSA observations project            | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| OCSW           | `#oscw-public:matrix.org <https://app.element.io/?#/room/#oscw-public:matrix.org>`_              | Open Source CubeSat Workshop                       | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| OSCW           | `#oscw:matrix.org <https://app.element.io/?#/room/#oscw:matrix.org>`_                            | OSCW organization committee                        | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| Polaris        | `#polaris:matrix.org <https://app.element.io/?#/room/#polaris:matrix.org>`_                      | Polaris machine learning project                   | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| Metasat        | `#metasat:matrix.org <https://app.element.io/?#/room/#metasat:matrix.org>`_                      | Metasat project                                    | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
| Cronos Rocket  | `#cronos-rocketry:matrix.org <https://app.element.io/?#/room/#cronos-rocketry:matrix.org>`_      | Cronos Rocketry project                            | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------+--------------+
