########
Contacts
########

********
Overview
********

The shared address books within LSF are centralized repositories of contact
information which can be accessed and utilized by multiple members and groups
of the organization.
These address books streamline communication and collaboration by ensuring that
contact details for contributors and other stakeholders are readily available
and up-to-date.
LSF uses Nextcloud as the tool for contact management since it integrates
seamlessly with the rest of the cloud collaboration services.

*************
Address books
*************

Contributors
============

The ``Contributors`` address book contains contact information of all active
LSF core contributors.
The address book is shared with read/write access to the ``Contributors``
group.
Each member of this group is individually responsible for keeping their contact
information current and up to date.

Contributors' contacts
======================

The ``Contributors' contact`` address book contains contact information of
people which is useful to active LSF core contributors.
The address book is shared with read and write access to the ``Contributors``
group.
All members of this group share the responsibility for ensuring the contacts in
this address book are kept tidy and well-organized.

Board
=====

The ``Board`` address book contains contact information of all the board
members.
The address book is shared with read access to the ``Contributors`` group, and
read/write access to the ``Board`` group.
Each member of the board is individually responsible for keeping their contact
information current and up to date.

Board's contacts
================

The ``Board's contacts`` address book contains contact information of people
which are useful to board members.
The address book is shared with read/write access to the ``Board`` group.
All members of the board share the responsibility for ensuring the contacts in
this address book are kept tidy and well-organized.
