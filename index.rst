####################################
Libre Space Foundation documentation
####################################

.. toctree::
   :maxdepth: 2
   :caption: 🚀 Projects
   :hidden:

   projects/what-is-an-lsf-project
   Management <projects/project-management>
   Portfolio <projects/projects-portfolio>
   Joining <projects/project-joining>
   Proposing <projects/project-proposing>
   projects/roles

.. toctree::
   :maxdepth: 2
   :caption: 👥 People
   :hidden:

   Overview <people/people-overview>
   people/board
   people/core-contributors
   people/paid-contributors
   HR Team <people/human-resources-team>
   Service Desk <people/human-resources-service-desk>
   people/hiring

.. toctree::
   :maxdepth: 2
   :caption: 🐝 Collaboration
   :hidden:

   collaboration/decision-making
   collaboration/meetings
   collaboration/conflict-resolution
   collaboration/code-of-conduct

.. toctree::
   :maxdepth: 2
   :caption: 📨 Communication
   :hidden:

   Channels <communication/communication-channels>
   communication/event-management
   communication/contacts

.. toctree::
   :maxdepth: 2
   :caption: ⚙️ Operation
   :hidden:

   operation/entity
   operation/facilities
   operation/procurement
   operation/accounting
   operation/shipping
   operation/travel
   operation/legal
   IT Infrastructure <operation/it-infrastructure-operations>

.. toctree::
   :maxdepth: 2
   :caption: 🪄 How-Tos / Guides
   :hidden:

   how-tos_guides/hw-development-guidelines
   Docs with GitLab Web IDE <how-tos_guides/docs-with-gitlab-web-ide>

.. toctree::
   :maxdepth: 2
   :caption: 📚 References
   :hidden:

   glossary
   license

*****
Intro
*****

Welcome to Libre Space Foundation documentation.
In these pages you can find operational principles, definitions, processes, and
best practices for various internal workings of the Foundation.

* :ref:`search`
