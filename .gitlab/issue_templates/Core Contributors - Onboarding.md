# Core contributor onboarding #

## Prerequisites ##

- [ ] GitLab account
- [ ] PGP key ID
- [ ] An onboarding "buddy": *Replace this sentence with the GitLab username of your buddy*

## Actions ##

### Human resources ###

- [ ] Get informed on [how to make requests to HR](https://docs.libre.space/en/stable/people/paid-contributors.html#other-matters) or [announcing leaves](https://docs.libre.space/en/stable/people/paid-contributors.html#time-off)
- [ ] Open issue on HR service desk to [add your PGP key ID](https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new?issuable_template=Payslip%20-%20Update%20PGP%20key&issue[title]=Update%20payslip%20PGP%20key) into the payslip database (paid only)
- [ ] Get informed about [paid contributor benefits](https://docs.libre.space/en/stable/people/paid-contributors.html#benefits) (paid-only)

### Email ###

- [ ] Open issue on IT operations support to [create an LSF email account](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=Email%20-%20Create%20account&issue[title]=Create%20email%20account). Request to be added to the aliases:
  - `all@libre.space`, if based outside of Greece
  - `greece@libre.space`, if based in Greece
  - `staff@libre.space`, if paid-contributor

### GitLab ###

- [ ] Open issue on IT operations support to [be added as a developer](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=GitLab%20-%20Modify%20project%20developers&issue[title]=Modify%20GitLab%20project%20developers) to GitLab projects:
  - `librespacefoundation/lsf-hr-leaves`, if paid-contributor
  - `librespacefoundation/lsf-core`
  - `librespacefoundation/lsf-org`

### Matrix ###

- [ ] Join `#lsf:matrix.org`
- [ ] Open issue on IT operations support to [be invited to `#lsf-core:matrix.org` channel](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=Matrix%20-%20Invite%20user%20to%20invite-only%20channel&issue[title]=Invite%20user%20to%20matrix%20invite-only%20channel)

### Community ###

- [ ] Create an account at the [Community Forum](https://community.libre.space)
- [ ] Open issue on HR service desk to [become a member of `lsf-core` community group](https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new)
- [ ] Open issue on [Libre Space Communications service desk](https://gitlab.com/librespacefoundation/lsf-comms-servicedesk) for adding you in the contributors list on `About Us` page of [libre.space](https://libre.space) website

### Cloud ###

- Open issues on IT operations support to:
  - [ ] [Create a Cloud user](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=Cloud%20-%20Create%20user&issue[title]=Create%20Cloud%20user). Request to be added to groups:
    - `Contributors`
    - `Staff` (for paid-only)
  - [ ] Update personal info on your [LSF Cloud](https://cloud.libre.space/settings/user) account, including phone number and location
  - [ ] Update your vCard entry on [LSF Cloud](https://cloud.libre.space/apps/contacts/), including phone number, location and PGP key

### Travel ###

- [ ] Read through the [Travel policy](https://docs.libre.space/en/stable/operation/travel.html) and create a airbnb account (see the policy for instructions)

### Procurement ###

- [ ] [Create an Amazon Business account](https://www.amazon.de/ab/invitations/accept/2023120UpCetEMiTwONDc3UdXKD4Q) using your LSF email

### LSF projects ###

- [ ] Overview running LSF projects by visiting the [LSF Core Kanban board](https://gitlab.com/librespacefoundation/lsf-core/-/boards/3342848) and [LSF Documentation](https://docs.libre.space/en/stable/projects/projects-portfolio.html#currently-active-projects)
- [~] Get informed on [how to join a project](https://docs.libre.space/en/stable/projects/FIXME)
off)

### Meetings ###

- [ ] Get informed about [regular LSF meetings](https://docs.libre.space/en/stable/collaboration/meetings.html#regular-lsf-meetings)

/confidential
