###############
Project joining
###############

.. contents::
   :local:
   :backlinks: none

********
Overview
********

Thank you for your interest in joining Libre Space Foundation as a project.

This document provides a simple list of the LSF expectations of any Libre
Space project, with pointers to the detailed policies or best practices.
"Libre Space project" specifically means a top level project at the LSF.

#. Some things projects MUST do are requirements; failure to follow those
   results in the board taking action to correct the issue.
#. There are many best practices that projects SHOULD do. Broad experience at
   the LSF shows these are proven methods that work well.
#. The LSF and some Libre Space projects have other suggestions and practices
   that projects MAY do.

Ultimately, Libre Space projects report to, and are responsible to, the Libre
Space Foundation Board, which mandates these policies for the LSF as a whole.

**********
Governance
**********

#. Project decisions and direction MUST adhere to the Libre Space Manifesto.
#. Project technical decisions MUST be made and communicated on public and
   archived places.
#. Project discussions and interactions SHOULD be held in public in accessible,
   asynchronous and archived places.
#. Project discussions SHOULD use normal LSF-hosted communication channels.
#. Projects MAY use a documented consensus process or a VOTE for any new
   committers or project manager members, and carefully follow policies for
   granting access.
#. Projects MUST provide a quarterly status report to the Board.
#. Projects MUST govern themselves independently of undue commercial influence,
   and for the best interests of the project community as a whole.

*********
Technical
*********

#. The primary source control repository MUST be administered by the LSF Infra
   team on LSF organizational accounts.
#. Project SHOULD follow available tests and validation, preferably included
   in a continuous integration process.
#. Project SHOULD use common LSF projects set-up.
#. Projects SHOULD use fully open source tool chains: all tools used in a Libre
   Space project SHOULD be open source and compatible to avoid locks.

*****
Legal
*****

#. Projects MUST use an open source copyleft license for anything developed and
   released within the project, clearly marked in the source repository
   including LICENSE, and source headers.
#. Projects MUST NOT include software with unapproved or restricted licenses in
   Libre Space project releases unless following explicit exceptions.
#. Projects MAY include software with approved compatible licenses in Libre
   Space project releases.

*****
Brand
*****

#. Project websites and materials MUST comply with the Libre Space Project
   Branding Requirements.
#. Projects MAY request trademark registration and ownership by Libre Space
   Foundation.

*****************
Press & marketing
*****************

#. Projects MUST work with Board Champion and Communications team on any formal
   press releases.
#. Projects SHOULD work with LSF Communications team to help coordinate any
   press, media, or analyst relations, or for marketing assistance.

***************
How LSF assists
***************

For all approved LSF projects the following support applies:

#. LSF SHOULD provide all needed infrastructure and tools for project
   development, deployment and operations. (for example repositories,
   discussion forums, servers)
#. LSF SHOULD sponsor the creation of a brand identity if the project is
   missing one.
#. LSF SHOULD provide legal guidance and help for a project.
#. LSF MUST provide governance, management and development guidance as
   requested by a project.
#. LSF MUST evaluate funding applications as submitted by a project and, if
   approved by the board, provide the funding and support around
   it. (for example development funds, internships, travel funding, event
   funding, tools and hardware).
#. LSF Board Champion of the project MUST work closely with the project manager
   or committee to ensure timely goal execution and smooth project
   development.

***************
Joining process
***************

#. A project interested in joining Libre Space Foundation should review the
   conditions preceding and note possible incompatibilities or points needing
   clarification and/or discussion.
#. The project should initiate contact with the Board sending an email directly
   to a board member or board@ expressing interest.
#. The Board appoints a Board Champion to work with the project ensuring
   compatibility of the application.
#. The Board votes on accepting the project as a Libre Space Foundation
   project.
#. All necessary actions regarding project transition to LSF Project are done
   by the Board Champion and the Libre Space Foundation Operations team.
#. Communications team coordinates the announcement of the project joining LSF.
