###############
Decision making
###############

.. contents::
   :local:
   :backlinks: none

********
Overview
********

Decision-making and collaboration are two essential aspects of any successful
team or organization.
When working together, individuals must be able to make informed choices,
communicate effectively, and work together towards a common goal.
Effective decision-making involves a process of gathering information,
analyzing options, and ultimately selecting a course of action.
Advice-based and consensus-based decision-making are the standard practices for
all projects at Libre Space Foundation, excluding the LSF Board project, in
which the decision-making model is not defined yet.
As an organization that operates under self-management principles, the
consensus-based decision-making process shall only be employed in cases where
the advice-based decision-making cannot be followed.

*************
Prerequisites
*************

Before initiating any decision-making process, the following prerequisites need
to be established:

Communication
=============

One of the key aspects of effective decision-making is communication.
Teams must be able to communicate openly and honestly, sharing ideas and
feedback in a constructive manner.
This requires active listening skills, empathy, and a willingness to compromise
and work towards solutions that work for everyone involved.

Trust
=====

Another important aspect is trust.
Team members must feel comfortable sharing their opinions and ideas without
fear of judgement or criticism.
Building trust requires transparency, empathy, and a commitment to the larger
goal.

Adaptability
============

Lastly, decision-making requires a willingness to take risks and adapt to
changing circumstances.
Teams must be open to new ideas and willing to pivot or change course if
necessary to achieve their objectives.

****************************
Advice-based decision-making
****************************

Advice-based decisions are self-directed decisions that are made by individuals
based on the guidance, input, or recommendations of others.
It is a lightweight and fast process originating from do-ocratic and
self-management principles with increased decision-making performance and
efficiency.
Nevertheless, this decision-making process is not suitable in cases where:

* Self-confidence to take own-decision is low.
* Decision is considered high risk.
* Impact to the organization or project in case of failure is considered high.
* There are known potential objections by other team members.

Rules
=====

The key rules for advice-based decision-making are:

* Advice shall be sought from experts.
* Advice shall be sought from the people that are most directly impacted by
  the decision.

Workflow
========

.. graphviz::
   :align: center

   digraph workflow {
     node [fontname = "Helvetica"];
     edge [fontname = "Helvetica"];

     issue [
       group = column1;
       label = "Issue identified";
       shape = oval;
     ];
     assign [
       group = column1;
       label = "Self-assign issue";
       shape = rect;
     ];
     info [
       group = column1;
       label = "Gather information";
       shape = rect;
     ];
     check_confidence [
       group = column1;
       label = "Self-confident\nto take own-decision?";
       shape = diamond;
     ];
     impact [
       group = column1;
       label = "Evaluate impact to\norganization or project\nin case of failure";
       shape = rect;
     ];
     check_impact [
       group = column1;
       label = "Is impact\nconsidered high?";
       shape = diamond;
     ];
     experts_advice [
       group = column1;
       label = "Seek advice from experts";
       shape = rect;
     ];
     impacted_advice [
       group = column1;
       label = "Seek advice from people\ndirectly impacted by\nthe decision.";
       shape = rect;
     ];
     proposal [
       group = column1;
       label = "Formulate proposal\nand course of action";
       shape = rect;
     ];
     check_risk [
       group = column1;
       label = "Is proposal risk\nhigh wrt. impact?";
       shape = diamond;
     ];
     objections [
       group = column1;
       label = "Evaluate possibility of\nobjections based on\nprevious experience";
       shape = rect;
     ];
     check_objections [
       group = column1;
       label = "Are objections\nhighly probable?"
       shape = diamond;
     ];
     implement [
       group = column1;
       label = "Implement proposal";
       shape = oval;
     ];
     consensus [
       group = column2;
       label = "Proceed to consensus\ndecision-making process";
       shape = oval;
     ];

     issue -> assign;
     assign -> info;
     info -> check_confidence;
     check_confidence:e -> consensus:n [ label = "No" ];
     check_confidence -> impact [ label = "Yes" ];
     impact -> check_impact;
     check_impact:e -> consensus:nw [ label = "Yes" ];
     check_impact -> experts_advice [ label = "No" ];
     experts_advice -> impacted_advice;
     impacted_advice -> proposal;
     proposal -> check_risk;
     check_risk:e -> consensus:sw [ label = "Yes" ];
     check_risk -> objections [ label = "No" ];
     objections -> check_objections;
     check_objections -> implement;
     check_objections:e -> consensus:s [ label = "Yes" ];

     {
       rank=same;
       impacted_advice; consensus;
     }
   }

*******************************
Consensus-based decision-making
*******************************

Consensus-based decision-making is a collaborative decision-making process that
seeks to reach agreement among all team members.
In this approach, all members of the group participate in the decision-making
process and work together to find a solution that everyone can support.

Rules
=====

The key rules for consensus decision-making are:

* All information and knowledge to make a decision shall be freely available.
  The exchange of information shall not be hindered in any way.
* Members shall actively listen to and consider all viewpoints and
  perspectives.
* Agreed ideas and decisions shall belong to the team as a whole and not to
  individual members, even if the decisions were made using passive acceptance
  or tolerance by these members.
* In principle, disagreements can always be resolved with enough discussion.
* Consensus may be reached by composing all opinions as long as they do not
  conflict with each other.
* The right to object shall always be respected.
* Discussion logs shall be maintained to the extent possible and especially in
  case where objections have been expressed.

Reaching consensus
==================

Participation in the discussion process is expected from all stakeholders.
Consensus is reached in any of the following cases:

* **Unanimity** - All members fully agree with the decision.
* **Acceptance** - Some members fully agree with the decision while the rest
  are neutral or have no opinion on the matter, effectively passively accepting
  the decision.
* **Tolerance** - Some members agree or accept the decision while the rest have
  objections but decide to tolerate it.
  In this case, the objections must be recorded.

There may be cases in which consensus is hindered by lack of participation from
objecting parties.
Therefore the following cases are considered as passive acceptance of the
outcome of the decision-making process:

* **Refusal to Participate** - Declining to engage in the discussion process
  without justified reason.
* **Unjustified Absence** - Failure to participate in scheduled discussions
  without valid justification.

Deadlines on initiating discussions
===================================

When an objection to a proposal or decision is raised, it's crucial to address
it promptly and constructively.
To facilitate this:

1. **Deadlines for Discussions**:
   Specific deadlines for initiating discussions to reach consensus must be
   defined immediately after an objection is recognized.
2. **Case-by-Case Consideration**:
   The deadlines shall be determined on a per-case basis, considering several
   key aspects to ensure fairness and efficiency.

Key aspects for deadline determination
--------------------------------------

The following (non-exhaustive) list of aspects should be considered when
setting deadlines for discussions:

* **Project Timeline Constraints** - How does the decision affect the overall
  project timeline and milestones?
* **Availability of People** - Are all necessary stakeholders available to
  participate in the discussion within a reasonable time-frame?
* **Impact of Decision** - What implications does the decision have on the
  project or organization?
* **Dependencies of Decision** - How does this decision affect other parts of
  the project?
* **Importance of Decision** - How inherently important is the decision itself?

Workflow
========

.. graphviz::
   :align: center

   digraph workflow {
     node [fontname = "Helvetica"];
     edge [fontname = "Helvetica"];

     start [
       group = column1;
       label = "Start";
       shape = oval;
     ];
     discuss [
       group = column1;
       label = "Discuss issue";
       shape = rect;
     ];
     proposal [
       group = column1;
       label = "(Re)formulate proposal\nand course of action";
       shape = rect;
     ];
     check_unanimity [
       group = column1;
       label = "Is there\nunanimity?"
       shape = diamond;
     ];
     check_acceptance [
       group = column1;
       label = "Is there\nacceptance?"
       shape = diamond;
     ];
     id_objection [
       group = column1;
       label = "Identify objections"
       shape = rect;
     ];
     check_tolerance [
       group = column1;
       label = "Is there\ntolerance?"
       shape = diamond;
     ];
     rec_objection [
       group = column1;
       label = "Record objections"
       shape = rect;
     ];
     implement [
       group = column1;
       label = "Implement proposal";
       shape = oval;
     ];

     start -> discuss;
     discuss -> proposal;
     proposal -> check_unanimity;
     check_unanimity -> check_acceptance [ label = "No" ];
     check_unanimity:w -> implement:w [ label = "Yes", weight = -1 ];
     check_acceptance -> id_objection [ label = "No" ];
     check_acceptance:w -> implement:wn [ label = "Yes", weight = -1 ];
     id_objection -> check_tolerance;
     check_tolerance -> rec_objection [ label = "Yes" ];
     check_tolerance:e -> discuss:e [ label = "No", weight = -1 ];
     rec_objection -> implement;

   }

..
   Some notes:

   * small group shall practice unanimity and not modified majority vote
   * continuous collaboration for seeking consensus
   * identify objection (or potential objection based on previous communication
     for other matters)
   * change proposal until consensus is reached
   * blocking is defined as the stall of the process due to lack of feedback
     for reaching consensus

Common pitfalls
===============

Inaction to decide
------------------

This occurs when the team cannot reach a consensus decision and can lead to a
prolonged and unproductive decision-making process that can demotivate and
frustrate team members.
Inaction to decide can be caused by a lack of information, resistance to change
or fear of failure.

Vulnerability to malicious members
----------------------------------

Malicious team members can intentionally disrupt the decision-making process
for their gain by continuously blocking decisions or advocating for decisions
that serve their self-interest rather than the interest of the team.
This can lead to a sense of distrust or resentment within the group.
Another tactic that a malicious member can use to steer or block decisions is
to dominate or hijack the discussions, leaving no room for other ideas to be
expressed.
Withholding information is also a common tactic used by malicious team members
to manipulate decisions.

Frustration and Demotivation
----------------------------

Consensus decision-making, while democratic and inclusive, can be a
time-consuming process.
This can sometimes demotivate and frustrate team members.

Low-quality decisions
---------------------

To achieve consensus, some members may reach a compromise or a composition of
ideas that may not represent the ideal outcome for the team or may not solve
the problem entirely and thus, conclude to a low-quality decision.

Groupthink
----------

Groupthink is a phenomenon that can arise during consensus decision-making.
It occurs when the desire for group harmony or cohesiveness results in a
decision that is not well thought-out or objective.
The pressure from the team can lead to individuals conforming to group norms or
not speaking out, which can result in suboptimal or even dangerous decisions.

Member isolation
----------------

In a consensus decision-making process, it is important to ensure that all
members feel heard and included in the decision-making process.
If an objecting member is isolated or ignored, it can cause issues within the
team and undermine the effectiveness of the consensus decision.

Fallback to other decision-making processes
-------------------------------------------

Excessive use of the right to block decisions may lead to major frustration.
In an attempt to unblock decisions, team members may be tempted to fallback to
majority vote and in practice violate the consensus-based decision-making.
