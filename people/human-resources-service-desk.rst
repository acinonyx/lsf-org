############################
Human resources service desk
############################

.. contents::
   :local:
   :backlinks: none

********
Overview
********

The Human Resources Service Desk serves as the primary contact point for paid
contributors seeking assistance with HR-related inquiries, concerns, and
requests.
Such requests include HR matters such as benefits, payroll, leaves, and hiring.

****************
Sending requests
****************

The Human Resources Service Desk offers multiple channels for submitting
requests.
`GitLab issues
<https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new>`_ can
be used for making requests, and a set of predefined issue templates for
commonly requested matters are available for ease of use:

* Benefits

  * Mobile phone plan

    * `Request <https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new?issuable_template=Benefit%20-%20Request%20mobile%20plan&issue[title]=Request%20mobile%20phone%20plan>`_
    * `Cancel <https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new?issuable_template=Benefit%20-%20Cancel%20mobile%20plan&issue[title]=Cancel%20mobile%20phone%20plan>`_

.. _hr-servicedesk-hiring:

* Hiring

  * Greece

    * `Employee <https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new?issuable_template=Hire%20-%20Employee%20in%20Greece&issue[title]=Hiring%20as%20an%20employee%20in%20Greece>`_

* Payslip

  * `Update PGP key <https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new?issuable_template=Payslip%20-%20Update%20PGP%20key&issue[title]=Update%20payslip%20PGP%20key>`_

For non-standard matters, paid contributors have the flexibility to provide
details in free-text format or `email <mailto:hr@libre.space>`_.
