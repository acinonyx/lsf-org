####################
Human resources team
####################

********
Overview
********

The purpose of a Human Resources is to support the organization and
specifically its core, paid, and candidate contributors, on procedural matters
related to recruitment, employment, labor laws compliance, and regulations,
benefits, etc.
The Human Resources team, is a group of people responsible for managing all
these aspects of Human Resources functions.

*****
Roles
*****

Each individual in the team self-assumes a set of roles for themselves
following the `Decision making process of Libre Space Foundation
<https://docs.libre.space/en/stable/collaboration/decision-making.html>`_.
These roles have specific responsibilities based on the current needs.
The roles are documented in the following table:

+--------------------+------------------------+----------------------------------------+
| Role               | Person                 | Description                            |
+====================+========================+========================================+
| Benefits           | .. gitlabuser::        | Manages paid contributors benefit      |
| Administrator      |    :username: dpa2deas | requests.                              |
+--------------------+------------------------+----------------------------------------+
| Compensation       | .. gitlabuser::        | Analyzes the organization's            |
| Analyst            |    :username: acinonyx | compensation structure compared to the |
|                    +------------------------+ labor market norms and proposes        |
|                    | .. gitlabuser::        | changes and compensation policies to   |
|                    |    :username: dpa2deas | attract paid contributors to the       |
|                    +------------------------+ organization.                          |
|                    | .. gitlabuser::        |                                        |
|                    |    :username: elkosmas |                                        |
|                    +------------------------+                                        |
|                    | .. gitlabuser::        |                                        |
|                    |    :username: pierros  |                                        |
|                    +------------------------+                                        |
|                    | .. gitlabuser::        |                                        |
|                    |    :username: papamat  |                                        |
+--------------------+------------------------+----------------------------------------+
| Documentation      | .. gitlabuser::        | Maintains systems for correct rendering|
| engineer           |    :username: acinonyx | of documentation from document source  |
|                    |                        | code.                                  |
+--------------------+------------------------+----------------------------------------+
| Employment         | .. gitlabuser::        | Oversees compliance with employment    |
| Compliance Manager |    :username: acinonyx | laws, regulations, and requirements of |
|                    +------------------------+ the organization.                      |
|                    | .. gitlabuser::        |                                        |
|                    |    :username: dpa2deas |                                        |
+--------------------+------------------------+----------------------------------------+
| Exit Facilitator   | .. gitlabuser::        | Oversees and facilitates the process of|
|                    |    :username: acinonyx | a paid contributor to revert to a      |
|                    |                        | non-paid status.                       |
+--------------------+------------------------+----------------------------------------+
| Hiring Interviewer | .. gitlabuser::        | Conducts interviews and evaluates      |
|                    |    :username: acinonyx | candidates during the hiring process.  |
|                    +------------------------+                                        |
|                    | .. gitlabuser::        |                                        |
|                    |    :username: pierros  |                                        |
|                    +------------------------+                                        |
|                    | .. gitlabuser::        |                                        |
|                    |    :username: papamat  |                                        |
+--------------------+------------------------+----------------------------------------+
| Hiring Manager     | .. gitlabuser::        | Executes the hiring process.           |
|                    |    :username: acinonyx |                                        |
|                    +------------------------+                                        |
|                    | .. gitlabuser::        |                                        |
|                    |    :username: dpa2deas |                                        |
+--------------------+------------------------+----------------------------------------+
| Kanban Board       | .. gitlabuser::        | Manages and facilitates the use of     |
| Coordinator        |    :username: acinonyx | Kanban boards within a team.           |
+--------------------+------------------------+----------------------------------------+
| Payroll            | .. gitlabuser::        | Processes and manages paid contributors|
| Administrator      |    :username: dpa2deas | compensation, ensuring accurate and    |
|                    |                        | timely payroll processing, including   |
|                    |                        | calculating wages, deductions, and     |
|                    |                        | benefits, and issuing paychecks or     |
|                    |                        | direct deposits to employees.          |
+--------------------+------------------------+----------------------------------------+
| Project Champion   | .. gitlabuser::        | Follows closely and reports important  |
|                    |    :username: elkosmas | updates related to the progress of the |
|                    |                        | project.                               |
+--------------------+------------------------+----------------------------------------+
| Project Manager    | .. gitlabuser::        | Coordinates with the managers of other |
|                    |    :username: acinonyx | projects in the organization for       |
|                    |                        | efficient resource sharing and         |
|                    |                        | planning.                              |
+--------------------+------------------------+----------------------------------------+
| Technical Writer   | .. gitlabuser::        | Creates and delivers technical         |
|                    |    :username: acinonyx | documentation and written content that |
|                    +------------------------+ is clear, concise, and user-friendly,  |
|                    | .. gitlabuser::        | including documentation, instructions  |
|                    |    :username: pierros  | and job positions.                     |
+--------------------+------------------------+----------------------------------------+
