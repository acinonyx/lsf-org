######
Hiring
######

New core members in a paid capacity is a great way to expand LSF team,
abilities, passion, bandwidth, and culture.
In this page you can find information about the hiring process and how LSF does this a bit differently.

*******
General
*******

In LSF, recruitment is led by the team in need of a new member (specifically
the Project Manager), not by Human Resources team (which only takes care of the
practicalities).
Conversations with candidates tend to center around three topics: fit with the
role, fit with the organization, and fit with the purpose.
The last two are often considered more important, as in self-managing
organizations like LSF, there is much fluidity around roles.
Thus a prioritized list of topics is as follows:

1. Fit with purpose
2. Fit with organization
3. Briefing about needs
4. Drafting roles covering needs

A fundamental question is asked for any prospective new core member in paid
capacity:

::

   Do we sense that we're meant to journey together?

This question can only be meaningfully answered when conversations are rooted
in honesty and integrity, with a willingness to inquire deeply and openly.

*****
Roles
*****

Each Project Manager with their respective peers, defines roles (see
:doc:`../projects/roles`) for their projects in a regular cadence (some
projects Quarterly others Annually etc).

Every month a dedicated team meets to define priorities for the vacant roles
identified per project.
The criteria for prioritization are project urgency, timelines, financial
viability and other organization considerations.

This list is `published in a page <https://s.libre.space/roles-public>`_ and
can be seen by anyone interested.

.. note::
    **Roles and NOT Positions**

    LSF uses roles and does not follow the traditional position definition
    practice for recruiting/hiring.
    This essentially means that prospective candidates are applying to join
    a single or multiple teams, in a single or multiple roles.
    LSF actively encourages new candidates to propose *their own roles too*,
    based on their expertise, passion and ideas that fit LSF mission and
    vision.



*******
Process
*******

1. Prospective applicant reviews the `list of (vacant) roles
   <https://s.libre.space/roles-public>`_.
2. Applicant reaches out to project managers as stated in the
   :doc:`../projects/projects-portfolio`, to discuss general fit for the
   roles of interest.
3. Applicant sends an email to hr@libre.space with 4 key information:

    a. Roles interested in and how the applicant see themselves fit in them
       (experience, motivations etc)
    b. Applicant self-assessment for Level (see
       :doc:`../people/paid-contributors`)
    c. Applicant self-assessment for cultural fit in the organization
    d. Any additional ideas/roles the applicant would like to bring forward

4. HR team `opens an Interview issue
   <https://gitlab.com/librespacefoundation/lsf-hr-org/-/issues/new?issuable_template=Interview>`_
   in hr-org GitLab project and a Hiring Manager is assigned for a cultural fit
   interview.
5. After the cultural fit interview, the issue is updated with notes and the
   applicant is directed (or not) to further interviews as needed with project
   managers or prospective peers.
6. After each interview the issue is updated with notes.
7. The Hiring Manager ensures that an offer can be made if the following are
   all true:

    a. Is the candidate a good organization fit?
    b. Is the candidate a good fit for the roles applied?
    c. Is the candidate a good fit for the organization purpose?
    d. Is there available budget from the Accounting team for the level
       specified?

8. If the answer to all preceding is positive, the Hiring Manager `opens a Hiring issue
   <https://gitlab.com/librespacefoundation/lsf-hr-org/-/issues/new?issuable_template=Hire%20-%20New>`_.
9.  An offer is made to the candidate by the Hiring Manager.
10. If the offer is accepted, then the candidate creates a GitLab account and
    opens a hiring issue on the HR service desk by selecting
    the appropriate issue template based on the type of employment and their
    local legislative region. A list of available issue templates can be found
    :ref:`here <hr-servicedesk-hiring>`.
11. After the hiring process is completed, the onboarding process can begin
    (see :doc:`../people/core-contributors`).

***********
A good fit?
***********

.. note::
   Many of the following information and LSF hiring practices can be found in
   `Reinventing Organizations Wiki
   <https://reinventingorganizationswiki.com/en/theory/recruitment/>`_.

Fit for role
============

Assessing the fit in terms of skills, experience, and expertise remains an
important component of the recruitment process, especially for specific roles
requiring expertise.
Roles in self-managing organizations are exchanged very fluidly, though.
For that reason, the "fit for role" is often not considered to be paramount, as
it is likely that a person's roles might change quickly.
Self-managing organizations experience that when employees are motivated to
take on a new and challenging role, they pick up new skills and experience in
surprisingly little time.

Fit with the organization
=========================

A second area to explore in conversation is:

::

   Will the person blossom in the organization?

   Will he or she thrive in a self-organizing environment?

   Does the person feel aligned by the organization's values?

   Does he or she "click" with the colleagues?

Fit with purpose
================

.. note::
   LSF `Manifesto <https://manifesto.libre.space>`_ can act as a perfect guide
   for purpose. 🚀

Finally, is the person energized by the organizations’ purpose?
Is there something in the person's history that makes them resonate, makes them
want to serve this purpose at this moment in their life?
The discussion triggered by these questions can reach substantial depth and
help both the candidate and the organization learn more about themselves.
Recruitment becomes a process of self-inquiry as much as a process of mutual
assessment.
